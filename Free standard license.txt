A permanent RAVEN PRO Standard license is now free for non-commercial use to citizens of your country.  In a few minutes you should receive two emails from birds-ecommerce@cornell.edu, one with your order confirmation and the other with Raven Pro installation instructions.  If one or both of these emails do not arrive within 15 minutes, please check your spam folder.

You may find several features in the new Raven website useful, including the tutorial videos, knowledge base, and support request form. 

http://ravensoundsoftware.com/
 
The Bioacoustics Research Program offers a five-day introductory-level Sound Analysis Workshop at the Cornell Lab of Ornithology (Ithaca, NY) featuring Raven Pro 1.5. The workshop is offered twice per year (spring and fall), and is intended primarily for biologists interested in analysis, visualization, and measurement of animal sounds. The workshop covers basic principles of spectrographic analysis and measurement of animal sounds, as well as specific tools and techniques in Raven Pro. For more information, please go to:

http://www.birds.cornell.edu/brp/sound-analysis-workshop/
 
The Bioacoustics Research Program is pleased to announce the SWIFT, our new terrestrial autonomous recording unit. Our hope is that this unit will enable not only researchers, but also hobbyists, from all over the world to collect acoustics data in support of wildlife conservation efforts. For more information, see http://www.birds.cornell.edu/brp/swift/.
 
You can find guidance about how to cite Raven at http://www.birds.cornell.edu/brp/raven/RavenCitation.html.
 
Thank you, and all the best for your work.
 
Regards,
Mike
 
---------------
Raven Project 
Bioacoustics Research Program
Cornell Lab of Ornithology
159 Sapsucker Woods Road
Ithaca NY 14850
 
Raven sound analysis software:
http://store.birds.cornell.edu/Raven_s/20.htm
http://RavenSoundSoftware.com
 
Bioacoustics Research Program and Elephant Listening Project
http://www.birds.cornell.edu/brp/
http://www.listenforwhales.org/
http://www.birds.cornell.edu/brp/elephant/
http://www.facebook.com/BioacousticsResearchProgram/
