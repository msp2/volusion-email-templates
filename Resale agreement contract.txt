Good afternoon,

Please review the attached contract.

If you would like to move forward with this contract, we will need you to provide the following information:

Company Name
Company Address
Company Telephone Number
Authorized Agent�s Name
Authorized Agent�s Title

Once the contract is received, we can complete our part of the contract, and you will sign it, and then we will need the Director of our programs final signature.

Thank you,
Tish Klein
